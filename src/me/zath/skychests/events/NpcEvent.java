package me.zath.skychests.events;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.controllers.VillagerController;
import me.zath.skychests.gui.MainGui;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.*;

public class NpcEvent implements Listener {

    @EventHandler
    public void onEnderChestInteract(PlayerInteractEvent e) {
        if (!e.getPlayer().hasPermission("skychests.enderchest")) return;
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if (e.getClickedBlock().getType() != Material.ENDER_CHEST) return;

        e.setCancelled(true);
        e.getPlayer().openInventory(MainGui.getPlayerGui(e.getPlayer().getName().toLowerCase(), e.getPlayer()));
        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CHEST_OPEN, 1, 1);
    }

    @EventHandler
    public void manipulate(PlayerArmorStandManipulateEvent e) {
        if (!e.getRightClicked().isVisible()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onLogin(PlayerJoinEvent e) {
        SkyChests.getVillagerArrayList().forEach(villager -> villager.getHologram().showPlayer(e.getPlayer()));
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntityType() != EntityType.VILLAGER) return;
        if (VillagerController.isVillager((Villager) e.getEntity())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (e.getEntityType() != EntityType.VILLAGER) return;
        if (!(e.getDamager() instanceof Player)) return;
        if (!e.getDamager().hasPermission("skychests.delnpc")) return;
        if (((Player) e.getDamager()).getItemInHand().getType() != Material.BLAZE_ROD) return;
        if (!VillagerController.isVillager((Villager) e.getEntity())) return;

        me.zath.skychests.objects.Villager villager = VillagerController.get((Villager) e.getEntity());
        villager.kill();

        villager.getHologram().delete();

        villager.delete();
        VillagerController.remove(villager.getId());
        e.getDamager().sendMessage(Config.getMsg_NpcDel().replaceAll("%npc%", "" + villager.getId()));
    }

    @EventHandler
    public void onClick(PlayerInteractEntityEvent e) {
        if (e.getRightClicked().getType() != EntityType.VILLAGER) return;
        if (VillagerController.isVillager((Villager) e.getRightClicked())) {
            e.setCancelled(true);
            Player p = e.getPlayer();
            if (!p.hasPermission("skychests.clicknpc")) {
                p.sendMessage(Config.getCmd_NoPermission());
                return;
            }
            p.openInventory(MainGui.getPlayerGui(p.getName().toLowerCase(), p));
            p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
        }
    }

}
