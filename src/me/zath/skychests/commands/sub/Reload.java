package me.zath.skychests.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.commands.ICommand;
import org.bukkit.command.CommandSender;

public class Reload implements ICommand {

    private static String desc;

    @Override
    public void execute(CommandSender sender, String[] args, SkyChests plugin) {
        Config.load();
        sender.sendMessage(Config.getMsg_Reload());
    }

    public static void load(String string){
        desc = string;
    }

    @Override
    public boolean supportsConsole() {
        return false;
    }

    @Override
    public String getPermission() {
        return "skychests.reload";
    }

    @Override
    public String getDescription() {
        return desc;
    }
}
