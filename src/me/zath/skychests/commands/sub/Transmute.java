package me.zath.skychests.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.commands.ICommand;
import me.zath.skychests.controllers.TransmuteController;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

public class Transmute implements ICommand {

    private static String desc;

    @Override
    public void execute(CommandSender sender, String[] args, SkyChests skyChests) {
        // /skychests converter plugin
        if(args.length < 2){
            sender.sendMessage(Config.getMsg_PluginNotSpecified());
            return;
        }
        if(!Arrays.stream(TransmuteController.transmutables).filter(args[1]::equalsIgnoreCase)
            .findAny().isPresent()){
            sender.sendMessage(Config.getMsg_CantBeTransmutedYet());
            return;
        }

        TransmuteController transmuteController;
        if(args.length > 2)
            transmuteController = new TransmuteController(args[1], true);
        else
            transmuteController = new TransmuteController(args[1], false);

        if(transmuteController.check())
            transmuteController.transmute();
        else
            sender.sendMessage(Config.getMsg_TransmuteError());

        sender.sendMessage(Config.getMsg_Transmuted());
    }

    public static void load(String string){
        desc = string;
    }

    @Override
    public boolean supportsConsole() {
        return true;
    }

    @Override
    public String getPermission() {
        return "skychests.transmute";
    }

    @Override
    public String getDescription() {
        return desc;
    }
}
