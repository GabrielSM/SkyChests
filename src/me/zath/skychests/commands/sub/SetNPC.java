package me.zath.skychests.commands.sub;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.commands.ICommand;
import me.zath.skychests.controllers.VillagerController;
import me.zath.skychests.objects.Villager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetNPC implements ICommand{

    private static String desc;

    @Override
    public void execute(CommandSender sender, String[] args, SkyChests plugin) {
        Player p = (Player) sender;
        VillagerController.setLoc(p.getLocation());
        new Villager(p.getLocation());
        p.sendMessage(Config.getMsg_NpcSet().replaceAll("%npc%", "" + Integer.valueOf(SkyChests.getVillagerArrayList().size()-1)));
    }

    public static void load(String string){
        desc = string;
    }

    @Override
    public boolean supportsConsole() {
        return false;
    }

    @Override
    public String getPermission() {
        return "skychests.setnpc";
    }

    @Override
    public String getDescription() {
        return desc;
    }
}
