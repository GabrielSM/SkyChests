package me.zath.skychests.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import me.zath.skychests.utils.Reflection;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Holograms {

    private List<Object> entitylist = new ArrayList<>();
    private String[] Text;
    private Location location;
    private int count;

    Holograms(String[] Text, Location location) {
        this.Text = Text;
        this.location = location;
        create();
    }

    public void showPlayer(Player p) {
        for (Object armor : entitylist) {
            try {
                Object packet = Reflection.getConstructor_PacketPlayOutSpawnEntityLiving().newInstance(armor);
                Reflection.getMethod_SendPacket().invoke(Reflection.getConnection(p), packet);
            } catch (Exception ignored){}
        }
    }

    private void hidePlayer(Player p) {
        for (Object armor : entitylist) {
            try {
                int nmsId = (int)Reflection.getMethod_GetId().invoke(armor);
                int[] id = {nmsId};

                Object packet = Reflection.getConstructor_PacketPlayOutEntityDestroy().newInstance(id);

                Reflection.getMethod_SendPacket().invoke(Reflection.getConnection(p), packet);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    void showAll() {
        SkyChests.getSkychests().getServer().getOnlinePlayers().forEach(this::showPlayer);
    }

    public void hideAll() {
        SkyChests.getSkychests().getServer().getOnlinePlayers().forEach(this::hidePlayer);
    }

    private void create() {
        double DISTANCE = 0.25D;
        for (String Text : this.Text) {
            Object entity = null;
            try {
                entity = Reflection.getConstructor_EntityArmorStand().newInstance(Reflection.getMethod_GetHandle().invoke(this.location.getWorld()), this.location.getX(), this.location.getY(), this.location.getZ());
                Reflection.getMethod_SetCustomName().invoke(entity, Text);
                Reflection.getMethod_SetCustomNameVisible().invoke(entity, true);
                Reflection.getMethod_SetInvisible().invoke(entity, true);
                Reflection.getMethod_SetGravity().invoke(entity, false);
            } catch ( Exception ignored) {}
            entitylist.add(entity);
            this.location.subtract(0, DISTANCE, 0);
            count++;
        }

        for (int i = 0; i < count; i++) {
            this.location.add(0, DISTANCE, 0);
        }
        this.count = 0;
    }

    public void delete(){
        hideAll();
        entitylist.forEach(object -> {
            try {
                Reflection.getMethod_Die().invoke(object);
            } catch (Exception ignored) {}
        });
    }

}
