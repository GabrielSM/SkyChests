package me.zath.skychests.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.controllers.VillagerController;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.metadata.FixedMetadataValue;

public class Villager {

    private Location location;
    private Integer id;
    private Holograms hologram;
    private org.bukkit.entity.Villager entity;

    public Villager(Location location) {
        this.location = location;
        spawn();

        Holograms holograms = new Holograms(new String[]{Config.getVillager_Name()}, this.location);
        holograms.showAll();
        this.hologram = holograms;

        this.id = SkyChests.getVillagerArrayList().size();

        SkyChests.getVillagerArrayList().add(this);
    }

    private void spawn(){
        org.bukkit.entity.Villager villager = this.location.getWorld().spawn(this.location, org.bukkit.entity.Villager.class);
        VillagerController.removeAI(villager);
        this.entity = villager;
        villager.setProfession(org.bukkit.entity.Villager.Profession.FARMER);
        villager.setAdult();
        villager.setCanPickupItems(false);
        villager.setMetadata("skychests", new FixedMetadataValue(SkyChests.getSkychests(), true));

        if(!villager.getNearbyEntities(0.5, 1, 0.5).isEmpty())
            villager.getNearbyEntities(0.5, 1, 0.5).stream().filter(entity1 -> entity1 != villager && entity1.getType() != EntityType.PLAYER).forEach(Entity::remove);

    }

    public void kill(){
        this.entity.remove();
    }

    public void delete(){
        SkyChests.getVillagerArrayList().remove(this);
    }

    public Integer getId() {
        return id;
    }

    public Holograms getHologram() {
        return hologram;
    }

    public org.bukkit.entity.Villager getEntity() {
        return entity;
    }

}
