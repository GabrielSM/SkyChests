package me.zath.skychests.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.controllers.ChestController;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    public static void updateConfig(Plugin skyChests, String configGeral, String configPlugin){
        try{
            YamlConfiguration finalyml = new YamlConfiguration();
            try {
                finalyml.load(new File(skyChests.getDataFolder(), configGeral));
            } catch (Exception e) {
                e.printStackTrace();
            }
            FileConfiguration tempConfig = YamlConfiguration.loadConfiguration(skyChests.getResource(configPlugin));
            for(String key : tempConfig.getKeys(true)){
                Object obj = tempConfig.get(key);
                if(finalyml.get(key) != null) obj = finalyml.get(key);
                finalyml.set(key, obj);
            }
            finalyml.save(new File(skyChests.getDataFolder(), configGeral));
            finalyml.load(new File(skyChests.getDataFolder(), configGeral));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static Player getPlayer(String name){
        for(Player player : SkyChests.getSkychests().getServer().getOnlinePlayers())
            if(player.getName().equalsIgnoreCase(name)) return player;

        return null;
    }

    public static Double getFinalPrice(Player p){
        Double toReturn = Config.getChest_Cost();

        if(hasPermStartsWith(p, "skychests.multiplier.")){
            Double multiplier = Integer.parseInt(getPermStartsWith(p, "skychests.multiplier.").replaceAll("skychests.multiplier.", "")) / 100.0;
            toReturn = toReturn + (toReturn * multiplier * ChestController.getChestsAmount(p.getName()));
        }

        if(hasPermStartsWith(p, "skychests.sale.")){
            Double sale = Integer.parseInt(getPermStartsWith(p, "skychests.sale.").replaceAll("skychests.sale.", "")) / 100.0;
            toReturn = toReturn - (toReturn * sale);
        }

        return toReturn;
    }

    private static Boolean hasPermStartsWith(Player p, String perm){
        return getPermStartsWith(p, perm) != null;
    }

    private static String getPermStartsWith(Player p, String perm){
        for (PermissionAttachmentInfo permissionAttachmentInfo : p.getEffectivePermissions()) {
            String permission = permissionAttachmentInfo.getPermission();
            if (permission.startsWith(perm)) return permission;
        }

        return null;
    }

    public static int getMaxChests(String dono) {
        int toReturn = Config.getChest_DefaultChestAmount();

        Player p = null;

        for (Player player : SkyChests.getSkychests().getServer().getOnlinePlayers()) {
            if (player.getName().equalsIgnoreCase(dono)) {
                p = player;
                break;
            }
        }

        if(p == null) return 28;

        for (PermissionAttachmentInfo perm : p.getEffectivePermissions()) {
            String permission = perm.getPermission();
            if (!permission.startsWith("skychests.max.")) continue;

            String number = permission.replaceAll("skychests.max.", "");
            if (!isInt(number)) continue;

            toReturn = Integer.parseInt(number);
        }

        return Math.min(28, toReturn);
    }

    public static int getChestSize(int chests){
        int toReturn = 0;

        if(chests > 28) toReturn = 9*6;

        if(chests <= 28){
            toReturn = 9*6;

            if(chests <= 21){
                toReturn = 9*5;

                if(chests <= 14){
                    toReturn = 9*4;

                    if(chests <= 7){
                        toReturn = 9*3;
                    }
                }
            }
        }

        return toReturn;
    }

    public static int[] getFillSlots(int rows){
        int[] toReturn = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        switch (rows){
            case 3:
                for(int i = 17; i <= 26; i++){
                    toReturn = addInt(toReturn, i);
                }

                break;
            case 4:
                toReturn = addInt(toReturn, 17);
                toReturn = addInt(toReturn, 18);
                for(int i = 26; i <= 35; i++){
                    toReturn = addInt(toReturn, i);
                }

                break;
            case 5:
                toReturn = addInt(toReturn, 17);
                toReturn = addInt(toReturn, 18);
                toReturn = addInt(toReturn, 26);
                toReturn = addInt(toReturn, 27);
                for(int i = 35; i <= 44; i++){
                    toReturn = addInt(toReturn, i);
                }

                break;
            case 6:
                toReturn = addInt(toReturn, 17);
                toReturn = addInt(toReturn, 18);
                toReturn = addInt(toReturn, 26);
                toReturn = addInt(toReturn, 27);
                toReturn = addInt(toReturn, 35);
                toReturn = addInt(toReturn, 36);
                for(int i = 44; i <= 53; i++){
                    toReturn = addInt(toReturn, i);
                }

                break;
        }

        return toReturn;
    }

    private static int[] addInt(int[] array, int toAdd) {
        array  = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = toAdd;
        return array;
    }

    public static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static Boolean hasSpace(Inventory inventory){
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack itemStack = inventory.getItem(i);
            if (itemStack == null) return true;
        }

        return false;
    }

    public static int[] getFreeSpaces(Inventory inventory){
        int[] toReturn = new int[0];
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack itemStack = inventory.getItem(i);
            if (itemStack == null) toReturn = addInt(toReturn, i);
        }

        return toReturn;
    }

    public static Boolean isEmpty(Inventory inventory) {
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack itemStack = inventory.getItem(i);
            if (itemStack != null) return false;
        }

        return true;
    }

    public static int getSlot(int slot) {
        int toReturn = 0;

        if (slot < 17) {
            toReturn = slot - 10;
        } else if (slot > 18 && slot < 26) {
            toReturn = slot - 12;
        } else if (slot > 27 && slot < 35) {
            toReturn = slot - 14;
        } else if (slot > 36 && slot < 44) {
            toReturn = slot - 16;
        }

        return toReturn;
    }

    public static ItemStack makeItem(String name, List<String> info, int id, int data) {
        ItemStack item = new ItemStack(id, 1, (short) data);
        ItemMeta itemmeta = item.getItemMeta();
        itemmeta.setDisplayName(ChatColor.GOLD + name);
        ArrayList<String> itemLore = info.stream().map(string -> string.replaceAll("&", "§")).collect(Collectors.toCollection(ArrayList::new));
        itemmeta.setLore(itemLore);
        item.setItemMeta(itemmeta);
        return item;
    }

    public static ItemStack makeItem(int id, int data) {
        return new ItemStack(id, 1, (short) data);
    }

}
