package me.zath.skychests;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.commands.AdminCmd;
import me.zath.skychests.commands.Cmd;
import me.zath.skychests.controllers.ChestController;
import me.zath.skychests.controllers.VillagerController;
import me.zath.skychests.events.ClickEvent;
import me.zath.skychests.events.NpcEvent;
import me.zath.skychests.gui.ConfirmGui;
import me.zath.skychests.gui.IconGui;
import me.zath.skychests.gui.MainGui;
import me.zath.skychests.objects.Chest;
import me.zath.skychests.objects.Villager;
import me.zath.skychests.utils.Reflection;
import me.zath.skychests.utils.SQL;
import me.zath.skychests.utils.Utils;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

public class SkyChests extends JavaPlugin{

    private static SkyChests skychests;
    private static Economy eco = null;
    private static ArrayList<Villager> villagerArrayList = new ArrayList<>();
    private static ArrayList<Chest> chestArrayList = new ArrayList<>();

    @Override
    public void onEnable() {
        skychests = this;
        if(getServer().getPluginManager().getPlugin("Vault") == null){
            getServer().getConsoleSender().sendMessage("§4ERROR §cVault §fnot found");
            getServer().getConsoleSender().sendMessage("§4ERROR §fDisabling " + getName());
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        File file = new File(getDataFolder(), "config.yml");
        if (!(file.exists())) {
            try {
                saveResource("config.yml", false);
            } catch (Exception ignored) {
            }
        }
        saveDefaultConfig();
        Utils.updateConfig(this, "config.yml", "config.yml");
        Config.load();
        if (!Config.getSql_Enabled()) {
            File db = new File(getDataFolder(), "chests.db");
            if (!(db.exists())) {
                try {
                    saveResource("chests.db", false);
                } catch (Exception ignored) {}
            }
            SQL.db = db;
        }
        Reflection.loadClasses();
        Reflection.loadMethods();
        Reflection.loadConstructors();
        SQL.createTable();
        MainGui.load();
        IconGui.load();
        ConfirmGui.load();
        VillagerController.loadLocations();
        ChestController.loadChests();
        ChestController.start();
        AdminCmd.registerCommands();
        registerCommands();
        registerEvents();
        setupEconomy();
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        Bukkit.getConsoleSender().sendMessage("§6" + getDescription().getName() + " §8v.§6" + getDescription().getVersion() + " §8de§6 "
            + getDescription().getAuthors() + " §2Ativado");
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
    }

    @Override
    public void onDisable() {
        if(!SkyChests.getChestArrayList().isEmpty())
            SkyChests.getChestArrayList().forEach(ChestController::update);
        if(!villagerArrayList.isEmpty())
            villagerArrayList.forEach(entity -> {
                entity.kill();
                entity.getHologram().delete();
            });
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        Bukkit.getConsoleSender().sendMessage("§6" + getDescription().getName() + " §8v.§6" + getDescription().getVersion() + " §8de§6 "
            + getDescription().getAuthors() + " §4Desativado");
        Bukkit.getConsoleSender().sendMessage("§6<§8-----------------------------§6>");
        HandlerList.unregisterAll();
        if (Config.getSql_Enabled()) {
            try {
                if (SQL.getConnection() != null && !SQL.getConnection().isClosed()) {
                    SQL.closeConnection();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void registerCommands() {
        getServer().getPluginCommand("skychests").setExecutor(new AdminCmd());
        getServer().getPluginCommand("bau").setExecutor(new Cmd());
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new NpcEvent(), this);
        getServer().getPluginManager().registerEvents(new ClickEvent(), this);
    }

    private void setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null){
            eco = economyProvider.getProvider();
        }
    }

    public static SkyChests getSkychests() {
        return skychests;
    }

    public static Economy getEco() {
        return eco;
    }

    public static ArrayList<Villager> getVillagerArrayList() {
        return villagerArrayList;
    }

    public static ArrayList<Chest> getChestArrayList() {
        return chestArrayList;
    }
}
