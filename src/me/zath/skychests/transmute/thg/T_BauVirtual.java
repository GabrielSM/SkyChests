package me.zath.skychests.transmute.thg;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import me.zath.skychests.transmute.interfaces.SerializeMethods;
import me.zath.skychests.transmute.interfaces.SqlProperties;
import me.zath.skychests.transmute.interfaces.SqlTable;
import me.zath.skychests.transmute.interfaces.Transmutable;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.UnsupportedEncodingException;

public class T_BauVirtual implements Transmutable {

    @Override
    public String getPluginName() {
        return "T_BauVirtual";
    }

    @Override
    public boolean isSql() {
        File file = new File(SkyChests.getSkychests().getDataFolder().getAbsolutePath().replaceAll("SkyChests", getPluginName()), "configuracao.yml");
        if(!file.exists())
            throw new NullPointerException("configuracao.yml doesnt exist");

        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
        if(!fileConfiguration.contains("SQL.Enable"))
            throw new NullPointerException("couldn't find SQL.Enable in configuracao.yml");

        return fileConfiguration.getBoolean("SQL.Enable");
    }

    @Override
    public SqlProperties getSqlProperties() {
        if(!isSql())
            //return new SqlProperties("", "", 0, "", "", "armazenamento.yml");
            throw new NullPointerException("only .db files supported");

        File file = new File(SkyChests.getSkychests().getDataFolder().getAbsolutePath().replaceAll("SkyChests", getPluginName()), "configuracao.yml");
        if(!file.exists())
            throw new NullPointerException("configuracao.yml doesnt exist");

        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);

        String sqlHost;
        if(!fileConfiguration.contains("SQL.Host"))
            throw new NullPointerException("couldn't find SQL.Enable in configuracao.yml");
        else
            sqlHost = fileConfiguration.getString("SQL.Host");

        int sqlPort;
        if(!fileConfiguration.contains("SQL.Port"))
            throw new NullPointerException("couldn't find SQL.Enable in configuracao.yml");
        else
            sqlPort = fileConfiguration.getInt("SQL.Port");

        String sqlDatabase;
        if(!fileConfiguration.contains("SQL.Database"))
            throw new NullPointerException("couldn't find SQL.Enable in configuracao.yml");
        else
            sqlDatabase = fileConfiguration.getString("SQL.Database");

        String sqlUser;
        if(!fileConfiguration.contains("SQL.User"))
            throw new NullPointerException("couldn't find SQL.Enable in configuracao.yml");
        else
            sqlUser = fileConfiguration.getString("SQL.User");

        String sqlPassword;
        if(!fileConfiguration.contains("SQL.Password"))
            throw new NullPointerException("couldn't find SQL.Enable in configuracao.yml");
        else
            sqlPassword = fileConfiguration.getString("SQL.Password");

        return new SqlProperties(sqlUser, sqlHost, sqlPort, sqlDatabase, sqlPassword, "armazenamento.yml");
    }

    @Override
    public SqlTable getSqlTable() {
        return new SqlTable("bauvirtual", "player", "number", "icone", "inventario");
    }

    @Override
    public SerializeMethods getSerializeMethods() {
        return new Methods();
    }
}
