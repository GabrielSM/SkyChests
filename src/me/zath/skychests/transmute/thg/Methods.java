package me.zath.skychests.transmute.thg;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.transmute.interfaces.SerializeMethods;
import me.zath.skychests.utils.Serialize;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class Methods implements SerializeMethods {

    @Override
    public ItemStack[] toItemStackList(String string) {
        try {
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(Base64Coder.decodeLines(string));
            final BukkitObjectInputStream dataInput = new BukkitObjectInputStream(byteArrayInputStream);
            final ItemStack[] stacks = new ItemStack[dataInput.readInt()];

            for (int i = 0; i < stacks.length; i++) {
                final ItemStack itemStack = (ItemStack) dataInput.readObject();
                stacks[i] = itemStack;
            }

            dataInput.close();
            return stacks;
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Unable to load item stacks.", e);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ItemStack toItemStack(String string) {
        return Serialize.simpleDeserialize(string);
    }

}
