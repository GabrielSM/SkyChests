package me.zath.skychests.transmute.interfaces;
/*
 * MC 
 * Created by zAth
 */

public interface Transmutable {

    String getPluginName();

    boolean isSql();
    SqlProperties getSqlProperties();
    SqlTable getSqlTable();

    SerializeMethods getSerializeMethods();

}
