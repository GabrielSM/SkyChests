package me.zath.skychests.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import me.zath.skychests.objects.Chest;
import me.zath.skychests.transmute.interfaces.SerializeMethods;
import me.zath.skychests.transmute.interfaces.SqlProperties;
import me.zath.skychests.transmute.interfaces.SqlTable;
import me.zath.skychests.transmute.interfaces.Transmutable;
import me.zath.skychests.transmute.thg.T_BauVirtual;
import me.zath.skychests.utils.SQL;
import me.zath.skychests.utils.Serialize;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.sql.*;

public class TransmuteController extends BukkitRunnable {

    public static String[] transmutables = {"T_BAUVIRTUAL"};

    private Transmutable plugin;
    private boolean debug;

    private String pluginName;
    private boolean isSql;
    private SqlProperties sqlProperties;
    private SqlTable sqlTable;
    private SerializeMethods serializeMethods;

    public TransmuteController(String pluginName, boolean debug) {
        if (pluginName.equalsIgnoreCase("T_BAUVIRTUAL"))
            plugin = new T_BauVirtual();
        else
            throw new NullPointerException("Plugin cant be transmuted yet!");

        this.debug = debug;
    }

    public boolean check() {
        try {
            this.pluginName = plugin.getPluginName();
            this.isSql = plugin.isSql();
            this.sqlProperties = plugin.getSqlProperties();
            this.sqlTable = plugin.getSqlTable();
            this.serializeMethods = plugin.getSerializeMethods();

            if (this.debug) {
                SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName()
                    + this.pluginName);
                SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName()
                    + this.sqlProperties.toString());
                SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName()
                    + this.sqlTable.toString());
            }

            return true;
        } catch (NullPointerException nullPointerException) {
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage("§4" + nullPointerException.getMessage());
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage("§4" + nullPointerException.getMessage());
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage("§4" + nullPointerException.getMessage());

            return false;
        }
    }

    public void transmute() {
        this.runTaskAsynchronously(SkyChests.getSkychests());
    }

    @Override
    public void run() {
        Connection connection = null;
        if (this.isSql) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(
                    "jdbc:mysql://" + this.sqlProperties.getSqlHost() + ":" + this.sqlProperties.getSqlPort() + "/"
                        + this.sqlProperties.getSqlDatabase(), this.sqlProperties.getSqlUser(), this.sqlProperties.getSqlPassword());
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection("jdbc:sqlite:"
                    + SkyChests.getSkychests().getDataFolder().getAbsolutePath().replaceAll("SkyChests", this.pluginName)
                    + File.separator + this.sqlProperties.getLocalFileName());
            } catch (ClassNotFoundException | SQLException ex8) {
                ex8.printStackTrace();
            }
        }
        if(connection == null)
            throw new NullPointerException("Null connection");

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + sqlTable.getTableName());

            ResultSet rs = statement.executeQuery();
            int count = 0;
            while (rs.next()) {
                String owner = rs.getString(sqlTable.getOwnerColumn());
                int id = rs.getInt(sqlTable.getIdColumn());

                String serializedIcon = rs.getString(sqlTable.getSerializedIconColumn());
                ItemStack icon = serializeMethods.toItemStack(serializedIcon);
                String skyChestsSerializedIcon = Serialize.simpleSerialize(icon);

                String serializedContents = rs.getString(sqlTable.getSerializedContentsColumn());
                ItemStack[] contents;
                if (serializedContents == null)
                    contents = new ItemStack[]{new ItemStack(0)};
                else
                    contents = serializeMethods.toItemStackList(serializedContents);
                String skyChestsSerializedContents = Serialize.toBase64List(contents);

                Chest chest = new Chest(owner, id, skyChestsSerializedIcon, icon, skyChestsSerializedContents, contents);
                SQL.create(chest);

                if (debug) {
                    count++;
                    SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName()
                        + " §2Báu #" + chest.getId() + " de " + chest.getDono() + " com " + chest.getInventory().getContents().length + " items criado.");
                }

                Thread.sleep(100);
            }
            if (debug) {
                SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName()
                    + " " + count + " §2Báus criados");
            }

            statement.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();

            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Erro convertendo os baus");
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
