package me.zath.skychests.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import me.zath.skychests.objects.Villager;
import me.zath.skychests.utils.Reflection;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;

public class VillagerController {

    public static void remove(int l) {
        int vendedores = SkyChests.getSkychests().getConfig().getConfigurationSection("Npcs").getKeys(false).toArray().length - 1;
        for (int i = l; i < vendedores; i++) {
            SkyChests.getSkychests().getConfig().set("Npcs." + i + ".World", SkyChests.getSkychests().getConfig().getString("Npcs." + Integer.valueOf(i + 1) + ".World"));
            SkyChests.getSkychests().getConfig().set("Npcs." + i + ".X", SkyChests.getSkychests().getConfig().getInt("Npcs." + Integer.valueOf(i + 1) + ".X"));
            SkyChests.getSkychests().getConfig().set("Npcs." + i + ".Y", SkyChests.getSkychests().getConfig().getInt("Npcs." + Integer.valueOf(i + 1) + ".Y"));
            SkyChests.getSkychests().getConfig().set("Npcs." + i + ".Z", SkyChests.getSkychests().getConfig().getInt("Npcs." + Integer.valueOf(i + 1) + ".Z"));
            SkyChests.getSkychests().getConfig().set("Npcs." + i + ".Pitch", SkyChests.getSkychests().getConfig().getDouble("Npcs." + Integer.valueOf(i + 1) + ".Pitch"));
            SkyChests.getSkychests().getConfig().set("Npcs." + i + ".Yaw", SkyChests.getSkychests().getConfig().getDouble("Npcs." + Integer.valueOf(i + 1) + ".Yaw"));
        }
        SkyChests.getSkychests().getConfig().set("Npcs." + vendedores, null);
        SkyChests.getSkychests().saveConfig();
    }

    public static void setLoc(Location l){
        int vendedores;
        if(SkyChests.getSkychests().getConfig().getConfigurationSection("Npcs") != null) {
            vendedores = SkyChests.getSkychests().getConfig().getConfigurationSection("Npcs").getKeys(false).toArray().length;
        } else {
            vendedores = 0;
        }
        SkyChests.getSkychests().getConfig().set("Npcs." + vendedores + ".World", l.getWorld().getName());
        SkyChests.getSkychests().getConfig().set("Npcs." + vendedores + ".X", Math.round(l.getX()));
        SkyChests.getSkychests().getConfig().set("Npcs." + vendedores + ".Y", Math.round(l.getY()));
        SkyChests.getSkychests().getConfig().set("Npcs." + vendedores + ".Z", Math.round(l.getZ()));
        SkyChests.getSkychests().getConfig().set("Npcs." + vendedores + ".Pitch", l.getPitch());
        SkyChests.getSkychests().getConfig().set("Npcs." + vendedores + ".Yaw", l.getYaw());
        SkyChests.getSkychests().saveConfig();
    }

    public static void loadLocations(){
        int vendedores;
        if(SkyChests.getSkychests().getConfig().getConfigurationSection("Npcs") != null) {
            vendedores = SkyChests.getSkychests().getConfig().getConfigurationSection("Npcs").getKeys(false).toArray().length;
        } else {
            vendedores = 0;
        }
        for(int i = 0; i < vendedores;i++){
            World w = SkyChests.getSkychests().getServer().getWorld(SkyChests.getSkychests().getConfig().getString("Npcs." + i + ".World"));
            int x = SkyChests.getSkychests().getConfig().getInt("Npcs." + i + ".X");
            int y = SkyChests.getSkychests().getConfig().getInt("Npcs." + i + ".Y");
            int z = SkyChests.getSkychests().getConfig().getInt("Npcs." + i + ".Z");
            double pitch = SkyChests.getSkychests().getConfig().getDouble("Npcs." + i + ".Pitch");
            double yaw = SkyChests.getSkychests().getConfig().getInt("Npcs." + i + ".Yaw");
            Location l = new Location(w, x, y, z, (float)yaw, (float)pitch);
            new Villager(l);
        }
    }

    public static void removeAI(Entity bukkitEntity) {
        try {
            Object nmsEntity = Reflection.getMethod_CraftEntityGetHandle().invoke(bukkitEntity);
            Object tag = Reflection.getMethod_GetNbtTag().invoke(nmsEntity);
            if (tag == null) {
                tag = Reflection.getClass_NBTTagCompound().newInstance();
            }
            Reflection.getMethod_C().invoke(nmsEntity, tag);
            Reflection.getMethod_SetInt().invoke(tag, "NoAI", 1);
            Reflection.getMethod_F().invoke(nmsEntity, tag);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Villager get(int id){
        Villager toReturn = null;
        for(Villager villager : SkyChests.getVillagerArrayList()){
            if(villager.getId() == id) toReturn = villager;
        }
        return toReturn;
    }

    public static Villager get(org.bukkit.entity.Villager entity){
        Villager toReturn = null;
        for(Villager villager : SkyChests.getVillagerArrayList()){
            if(villager.getEntity().equals(entity)) toReturn = villager;
        }
        return toReturn;
    }

    public static Boolean isVillager(org.bukkit.entity.Villager entity) {
        return entity.hasMetadata("skychests") || get(entity) != null;
    }

}
