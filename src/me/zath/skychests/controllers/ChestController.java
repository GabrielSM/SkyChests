package me.zath.skychests.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.gui.utils.GuiItems;
import me.zath.skychests.objects.Chest;
import me.zath.skychests.utils.SQL;
import me.zath.skychests.utils.Serialize;
import me.zath.skychests.utils.Utils;
import org.bukkit.inventory.ItemStack;

public class ChestController {

    public static void loadChests(){
        SQL.initialize();
    }

    public static void update(Chest chest){
        if(Utils.isEmpty(chest.getInventory())) {
            chest.setSerializedContents(Serialize.toBase64List(new ItemStack[] {new ItemStack(0)}));
        } else {
            chest.setSerializedContents(Serialize.toBase64List(chest.getInventory().getContents()));
        }
        chest.setSerializedIcon(Serialize.simpleSerialize(chest.getIcon()));

        SQL.update(chest.getDono(), chest.getId(), chest.getSerializedIcon(), chest.getSerializedContents());
    }

    public static void start(){
        SkyChests.getSkychests().getServer().getScheduler().scheduleAsyncRepeatingTask(SkyChests.getSkychests(), () -> {
            if(SkyChests.getChestArrayList().isEmpty()) return;
            SkyChests.getChestArrayList().forEach(ChestController::update);
        }, 0, Config.getChest_SaveDelay() * 20);
    }

    public static Chest create(String dono, Integer id){
        Chest chest = new Chest(dono, id, Serialize.simpleSerialize(GuiItems.getOwned(id)), Serialize.toBase64List(new ItemStack[] {new ItemStack(0)}));
        SQL.create(chest);

        return chest;
    }

    public static Chest get(String dono, int id){
        Chest toReturn = null;
        for(Chest chest : SkyChests.getChestArrayList()){
            if((chest.getDono().equalsIgnoreCase(dono)) && (chest.getId() == id)) toReturn = chest;
        }

        return toReturn;
    }

    public static Boolean hasChests(String dono){
        for(int i = 0; i < 28; i++){
            if(hasChestId(dono, i)) return true;
        }

        return false;
    }

    public static Boolean hasChestId(String dono, int id){
        return get(dono, id) != null;
    }

    public static int getChestsAmount(String dono){
        int toReturn = 0;
        if(!hasChests(dono)) return toReturn;

        for(int id = 0; id < 28; id++){
            if(hasChestId(dono, id)) toReturn++;
        }

        return toReturn;
    }

}
