package me.zath.skychests.gui;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import me.zath.skychests.gui.utils.GuiItems;
import me.zath.skychests.gui.utils.GuiHolder;
import org.bukkit.inventory.Inventory;

public class ConfirmGui {

    private static Inventory inventory;

    public static void load(){
        inventory = SkyChests.getSkychests().getServer().createInventory(null, 9*3);
        for(int i = 0; i < 27; i++){
            if(i == 11){
                inventory.setItem(i, GuiItems.getBack());
            } else if(i == 15){
                inventory.setItem(i, GuiItems.getConfirm());
            } else {
                inventory.setItem(i, GuiItems.getFill());
            }
        }
    }

    public static Inventory getBuying(String dono, int id){
        Inventory toReturn = SkyChests.getSkychests().getServer().createInventory(new GuiHolder(dono, id, GuiHolder.Type.BUY), 9*3, me.zath.skychests.Config.getGui_ConfirmTitle());
        toReturn.setContents(inventory.getContents());
        return toReturn;
    }

    public static Inventory getDeleting(String dono, int id){
        Inventory toReturn = SkyChests.getSkychests().getServer().createInventory(new GuiHolder(dono, id, GuiHolder.Type.DELETE), 9*3, me.zath.skychests.Config.getGui_ConfirmTitle());
        toReturn.setContents(inventory.getContents());
        return toReturn;
    }

}
