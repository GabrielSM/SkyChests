package me.zath.skychests.gui;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.gui.utils.GuiHolder;
import me.zath.skychests.gui.utils.GuiItems;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

public class IconGui {

    private static HashMap<Integer, Inventory> inventorysMap = new HashMap<>();
    private static int maxPages;
    private static HashMap<String, Integer> pageCount = new HashMap<>();

    public static void load() {
        Inventory inv = SkyChests.getSkychests().getServer().createInventory(null, 6 * 9);
        for (int i = 36; i <= 53; i++) {
            switch (i) {
                case 46:
                    inv.setItem(i, GuiItems.getPrevious());
                    break;
                case 49:
                    inv.setItem(i, GuiItems.getBack());
                    break;
                case 52:
                    inv.setItem(i, GuiItems.getNext());
                    break;
                default:
                    inv.setItem(i, GuiItems.getFill());
                    break;
            }
        }
        int itemsPerPage = 0;
        for (int l = 0; l < inv.getSize(); l++) {
            if (inv.getItem(l) == null || inv.getItem(l).getType() == Material.AIR) {
                itemsPerPage += 1;
            }
        }

        ArrayList<ItemStack> availableIcons = GuiItems.getIcons();

        Double inventorys = (availableIcons.size() / itemsPerPage) + 0.5;
        maxPages = inventorys.intValue() + 1;

        for (int l = 0; l < maxPages; l++) {
            Inventory inventory = SkyChests.getSkychests().getServer().createInventory(null, 6 * 9, Config.getGui_IconTitle());
            inventory.setContents(inv.getContents());

            int inicio = l * itemsPerPage;
            int fim = (l * itemsPerPage) + itemsPerPage;
            int slot = 0;

            for (ItemStack itemStack : availableIcons) {
                if (slot >= inicio && slot <= fim) {
                    inventory.addItem(itemStack);
                }
                slot++;
            }

            inventorysMap.put(l, inventory);
        }

    }

    private static Inventory getInventory(int key) {
        return inventorysMap.get(key);
    }

    public static int getPlayerPage(Player p) {
        return pageCount.get(p.getName().toLowerCase());
    }

    public static int getMaxPages() {
        return maxPages;
    }

    public static void open(Player p, String dono, int id) {
        pageCount.put(p.getName().toLowerCase(), 0);
        displayGui(p, dono, id);
    }

    public static void nextPage(Player p, String dono, int id) {
        pageCount.put(p.getName().toLowerCase(), Integer.valueOf(pageCount.get(p.getName().toLowerCase()) + 1));
        displayGui(p, dono, id);
    }

    public static void previousPage(Player p, String dono, int id) {
        pageCount.put(p.getName().toLowerCase(), Integer.valueOf(pageCount.get(p.getName().toLowerCase()) - 1));
        displayGui(p, dono, id);
    }

    private static void displayGui(Player p, String dono, int id) {
        int page = pageCount.get(p.getName().toLowerCase());
        Inventory toCopy = getInventory(page);

        Inventory inv = SkyChests.getSkychests().getServer().createInventory(new GuiHolder(dono, id, GuiHolder.Type.ICON), toCopy.getSize(), toCopy.getName());
        inv.setContents(getInventory(page).getContents());

        p.openInventory(inv);
    }

}
