package me.zath.skychests.gui;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.controllers.ChestController;
import me.zath.skychests.gui.utils.GuiHolder;
import me.zath.skychests.gui.utils.GuiItems;
import me.zath.skychests.objects.Chest;
import me.zath.skychests.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class MainGui {

    private static Inventory mainGui;

    public static void load(){
        mainGui = SkyChests.getSkychests().getServer().createInventory(null, Utils.getChestSize(Config.getChest_Chests()));
        for (int slot : Utils.getFillSlots( Utils.getChestSize(Config.getChest_Chests()) / 9 )) {
            mainGui.setItem(slot, GuiItems.getFill());
        }
    }

    public static Inventory getPlayerGui(String player, Player toOpen){
        Inventory toReturn = SkyChests.getSkychests().getServer().createInventory(new GuiHolder(player.toLowerCase(), -1, GuiHolder.Type.MAIN), mainGui.getSize(), Config.getGui_Title().replaceAll("%jogador%", player));
        toReturn.setContents(mainGui.getContents());
        for(int i = 0; i < Config.getChest_Chests(); i++){
            if(ChestController.hasChestId(player.toLowerCase(), i)){
                Chest chest = ChestController.get(player.toLowerCase(), i);
                toReturn.addItem(GuiItems.getOwned(chest.getIcon().getTypeId(), chest.getIcon().getData().getData(),i));
            } else {
                toReturn.addItem(GuiItems.getNotOwned(i, player, toOpen));
            }
        }

        if (Utils.hasSpace(toReturn)){
            for(int slot : Utils.getFreeSpaces(toReturn)) toReturn.setItem(slot, GuiItems.getFill());
        }

        return toReturn;
    }

}
